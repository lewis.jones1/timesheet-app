package com.lewis.timesheetservice.configuration;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;

public class TimesheetConfiguration {
  @Bean
  public ModelMapper modelMapper() {
    return new ModelMapper();
  }
}
