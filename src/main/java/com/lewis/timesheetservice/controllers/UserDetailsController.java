package com.lewis.timesheetservice.controllers;

import com.lewis.timesheetservice.repository.UserDetailsRepository;
import com.lewis.timesheetservice.models.UserDetails;
import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/register")
@CrossOrigin(origins = "http://localhost:4200")   // Angular frontend on port 4200
public class UserDetailsController {
  private final UserDetailsRepository userDetailsRepository;

  public UserDetailsController(UserDetailsRepository userDetailsRepository) {
    this.userDetailsRepository = userDetailsRepository;
  }

  @GetMapping
  public List<UserDetails> getUserDetails() {
    return userDetailsRepository.findAll();
  }

  @PostMapping
  public void addUserDetails(@RequestBody UserDetails userDetails) {
    userDetailsRepository.save(userDetails);
  }
}
