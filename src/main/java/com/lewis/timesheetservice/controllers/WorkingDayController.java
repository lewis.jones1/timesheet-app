package com.lewis.timesheetservice.controllers;

import com.lewis.timesheetservice.models.WorkingDay;
import com.lewis.timesheetservice.repository.WorkingDayRepository;
import com.lewis.timesheetservice.service.WorkingDayService;
import java.io.IOException;
import java.sql.Date;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/workingday")
public class WorkingDayController {

  private final WorkingDayRepository workingDayRepository;

  Logger logger = Logger.getLogger(UserDetailsController.class.getName());

  public WorkingDayController(WorkingDayRepository workingDayRepository) {
    this.workingDayRepository = workingDayRepository;
  }

  @GetMapping
  public List<WorkingDay> getWorkingDays() {
    logger.info("Fetching all working days...");
    return workingDayRepository.findAll();
  }

  @GetMapping
  @RequestMapping("/{id}")
  public List<WorkingDay> getWorkingDaysByUserId(@PathVariable("id") long id) {
    return workingDayRepository.findWorkingDaysByUserId(id);
  }

  @GetMapping
  @RequestMapping("/{id}/{startDate}/{endDate}")
  public List<WorkingDay> getWorkingDaysByDateBetweenAndUserId(
      @PathVariable("startDate") Date startDate,
      @PathVariable("endDate") Date endDate,
      @PathVariable("id") long id
  ) {
    WorkingDayService workingDayService = new WorkingDayService();
    List<WorkingDay> workingDays = workingDayRepository
        .findWorkingDaysByDateBetweenAndUserId(startDate, endDate, id);
    workingDayService.calculateHoursWorked(workingDays);

    return workingDayRepository.findWorkingDaysByDateBetweenAndUserId(startDate, endDate, id);
  }

  @GetMapping
  @RequestMapping("/{id}/{date}")
  public WorkingDay getSingleWorkingDayByUserId(
      @PathVariable("id") long id,
      @PathVariable("date") Date date) {
    return workingDayRepository.findWorkingDayByDateAndUserId(date, id);
  }

  @GetMapping
  @RequestMapping("/{id}/{startDate}/{endDate}/export")
  public void exportTimesheetToCsv(
      @PathVariable("startDate") Date startDate,
      @PathVariable("endDate") Date endDate,
      @PathVariable("id") long id, HttpServletResponse response
  ) throws IOException {
    List<WorkingDay> workingDays = workingDayRepository
        .findWorkingDaysByDateBetweenAndUserId(startDate, endDate, id);
    WorkingDayService workingDayService = new WorkingDayService();
    workingDayService.exportGivenTimesheetToCsv(workingDays, response);
  }

  @PostMapping
  public void newWorkingDay(@RequestBody WorkingDay workingDay) {
    logger.info("Adding new day...");
    workingDayRepository.save(workingDay);
  }

  @PutMapping
  @ResponseStatus(HttpStatus.OK)
  public void editWorkingDay(@RequestBody WorkingDay workingDay) {
    logger.info("Updating...");
    workingDayRepository.save(workingDay);
  }
}
