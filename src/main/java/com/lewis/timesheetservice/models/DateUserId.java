package com.lewis.timesheetservice.models;

import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

public class DateUserId implements Serializable {
  private Date date;
  private long userId;

  public DateUserId() {
  }

  public DateUserId(Date date, long userId) {
    this.date = date;
    this.userId = userId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    DateUserId dateUserId = (DateUserId) o;
    return date.equals(dateUserId.date) && userId == dateUserId.userId;
  }

  @Override
  public int hashCode() {
    return Objects.hash(date, userId);
  }
}
