package com.lewis.timesheetservice.models;


import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import lombok.Data;

@Data
@Entity
@IdClass(DateUserId.class)
public class WorkingDay implements Serializable {
  @Id
  private Date date;
  @Id
  private long userId;
  private Time morningStart;
  private Time morningEnd;
  private Time afternoonStart;
  private Time afternoonEnd;

  private String credit;
  private String reason;
}
