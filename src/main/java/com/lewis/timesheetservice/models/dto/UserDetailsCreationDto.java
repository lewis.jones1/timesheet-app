package com.lewis.timesheetservice.models.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDetailsCreationDto {
  private String name;
  private String grade;
  private double weeklyHours;
  private double hourDifference;
  private String startDate;
}
