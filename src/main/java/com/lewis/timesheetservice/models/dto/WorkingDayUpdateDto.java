package com.lewis.timesheetservice.models.dto;

import java.sql.Date;
import java.sql.Time;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkingDayUpdateDto {
  private Date date;
  private long userId;
  private Time morningStart;
  private Time morningEnd;
  private Time afternoonStart;
  private Time afternoonEnd;
  private String credit;
  private String reason;
}
