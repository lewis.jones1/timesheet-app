package com.lewis.timesheetservice.models.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.sql.Date;
import java.sql.Time;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WorkingDayCreationDto {
  @JsonProperty("date")
  private Date date;
  private long userId;
  private Time morningStart;
  private Time morningEnd;
  private Time afternoonStart;
  private Time afternoonEnd;
  private String credit;
  private String reason;
}
