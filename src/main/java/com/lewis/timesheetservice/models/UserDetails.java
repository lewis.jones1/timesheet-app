package com.lewis.timesheetservice.models;

import java.sql.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Data;

@Entity
@Data
public class UserDetails {
  //Todo: convert weeklyHours double to Duration, calculate the duration worked during week and subtract to get 
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long userId;
  private String name;
  private String grade;
  private double weeklyHours;
  private double hourDifference;
  private Date startDate;
}
