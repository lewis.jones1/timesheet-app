package com.lewis.timesheetservice.service;

import com.lewis.timesheetservice.models.WorkingDay;
import com.lewis.timesheetservice.repository.UserDetailsRepository;
import com.lewis.timesheetservice.repository.WorkingDayRepository;
import java.io.IOException;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

public class WorkingDayService {


  public Duration calculateHoursWorked(List<WorkingDay> workingDays) {

    List<Duration> durations = new ArrayList<>();

    workingDays.forEach(a ->
        durations.add(
            Duration.between(a.getMorningStart().toLocalTime(), a.getMorningEnd().toLocalTime())
                .plus(
                    Duration.between(a.getAfternoonStart().toLocalTime(),
                        a.getAfternoonEnd().toLocalTime()))));

    Duration totalDuration = Duration.ofHours(0);
    for (Duration duration : durations) {
      totalDuration = totalDuration.plus(duration);
    }

    return totalDuration;
  }

  public void exportGivenTimesheetToCsv(List<WorkingDay> workingDays, HttpServletResponse response) throws IOException {
    response.setContentType("text/csv");
    DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
    String currentDateTime = dateFormatter.format(new java.util.Date());

    String headerKey = "Content-Disposition";
    String headerValue = "attachment; filename=timesheet_" + currentDateTime + ".csv";
    response.setHeader(headerKey, headerValue);

    ICsvBeanWriter csvWriter = new CsvBeanWriter(response.getWriter(),
        CsvPreference.STANDARD_PREFERENCE);
    String[] csvHeader = {"Date", "Morning Start", "Morning End", "Afternoon Start",
        "Afternoon End"};
    String[] nameMapping = {"date", "morningStart", "morningEnd", "afternoonStart", "afternoonEnd"};

    csvWriter.writeHeader(csvHeader);

    for (WorkingDay workingDay : workingDays) {
      csvWriter.write(workingDay, nameMapping);
    }

    csvWriter.close();
  }
}
