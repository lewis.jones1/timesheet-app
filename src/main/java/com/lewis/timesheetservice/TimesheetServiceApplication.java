package com.lewis.timesheetservice;

import com.lewis.timesheetservice.models.UserDetails;
import com.lewis.timesheetservice.models.WorkingDay;
import com.lewis.timesheetservice.models.dto.WorkingDayCreationDto;
import com.lewis.timesheetservice.models.dto.WorkingDayUpdateDto;
import com.lewis.timesheetservice.repository.UserDetailsRepository;
import com.lewis.timesheetservice.repository.WorkingDayRepository;
import java.sql.Date;
import java.sql.Time;
import javax.jws.WebParam.Mode;
import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TimesheetServiceApplication {

  public static void main(String[] args) {
    SpringApplication.run(TimesheetServiceApplication.class, args);
  }

//  @Bean
//  CommandLineRunner create(WorkingDayRepository workingDayRepository) {
//    return args -> {
//      WorkingDayCreationDto creation = new WorkingDayCreationDto();
//      creation.setUserId(1);
//      creation.setDate(Date.valueOf("2021-08-21"));
//      creation.setMorningStart(Time.valueOf("09:05:00"));
//      creation.setMorningEnd(Time.valueOf("13:10:00"));
//      creation.setAfternoonStart(Time.valueOf("14:00:00"));
//      creation.setAfternoonEnd(Time.valueOf("17:00:00"));
//
//      final ModelMapper modelMapper = new ModelMapper();
//      WorkingDay workingDay = modelMapper.map(creation, WorkingDay.class);
//
//      workingDayRepository.save(workingDay);
//      workingDayRepository.findAll().forEach(System.out::println);
//    };
//  }
//
//  @Bean
//  CommandLineRunner next(WorkingDayRepository workingDayRepository) {
//    return args -> {
//      WorkingDayUpdateDto update = new WorkingDayUpdateDto();
//      update.setUserId(1);
//      update.setDate(Date.valueOf("2021-08-21"));
//      update.setMorningStart(Time.valueOf("09:06:00"));
//      update.setMorningEnd(Time.valueOf("13:00:00"));
//      update.setAfternoonStart(Time.valueOf("14:05:00"));
//      update.setAfternoonEnd(Time.valueOf("17:00:00"));
//
//      final ModelMapper modelMapper = new ModelMapper();
//      WorkingDay workingDay = modelMapper.map(update, WorkingDay.class);
//
//      workingDayRepository.save(workingDay);
//      workingDayRepository.findAll().forEach(System.out::println);
//    };
//  }
//
//  @Bean
//  CommandLineRunner init(UserDetailsRepository userDetailsRepository) {
//    return args -> {
//      UserDetails userDetails = new UserDetails();
//      userDetails.setUserId(1);
//      userDetails.setGrade("EO");
//      userDetails.setHourDifference(3.6);
//      userDetails.setName("Lewis Jones");
//      userDetails.setWeeklyHours(37);
//      userDetails.setStartDate(Date.valueOf("2021-01-04"));
//
//      final ModelMapper modelMapper = new ModelMapper();
//      UserDetails userDetailsMapped = modelMapper.map(userDetails, UserDetails.class);
//
//      userDetailsRepository.save(userDetailsMapped);
//      userDetailsRepository.findAll().forEach(System.out::println);
//    };
//  }

}
