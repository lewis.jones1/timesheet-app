package com.lewis.timesheetservice.repository;

import com.lewis.timesheetservice.models.DateUserId;
import com.lewis.timesheetservice.models.WorkingDay;
import java.util.Date;
import java.util.List;
import org.hibernate.jdbc.Work;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkingDayRepository extends JpaRepository<WorkingDay, DateUserId> {
  List<WorkingDay> findWorkingDaysByUserId(long id);

  List<WorkingDay> findWorkingDaysByDateBetweenAndUserId(
      Date startDate,
      Date endDate,
      long userId
  );

  WorkingDay findWorkingDayByDateAndUserId(Date date, long userId);
}
