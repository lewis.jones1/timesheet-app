package com.lewis.timesheetservice.repository;

import com.lewis.timesheetservice.models.UserDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserDetailsRepository extends JpaRepository<UserDetails, Double> {

}
