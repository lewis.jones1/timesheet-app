package com.lewis.timesheetservice.service;

import static org.junit.jupiter.api.Assertions.*;

import com.lewis.timesheetservice.models.WorkingDay;
import java.sql.Time;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class WorkingDayServiceTest {

  @Test
  void testCalculateHoursWorked() {
    Duration expectedDuration = Duration.parse("PT16H");

    WorkingDay day1 = new WorkingDay();
    WorkingDay day2 = new WorkingDay();

    day1.setMorningStart(Time.valueOf("09:00:00"));
    day1.setMorningEnd(Time.valueOf("12:30:00"));
    day1.setAfternoonStart(Time.valueOf("13:00:00"));
    day1.setAfternoonEnd(Time.valueOf("17:30:00"));

    day2.setMorningStart(Time.valueOf("09:00:00"));
    day2.setMorningEnd(Time.valueOf("12:30:00"));
    day2.setAfternoonStart(Time.valueOf("13:00:00"));
    day2.setAfternoonEnd(Time.valueOf("17:30:00"));

    List<WorkingDay> workingDays = new ArrayList<>();
    workingDays.add(day1);
    workingDays.add(day2);

    WorkingDayService workingDayService = new WorkingDayService();
    Duration actualDuration = workingDayService.calculateHoursWorked(workingDays);

    assertEquals(expectedDuration, actualDuration);
  }
}
