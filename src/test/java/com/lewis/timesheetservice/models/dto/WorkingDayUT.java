package com.lewis.timesheetservice.models.dto;


import static org.junit.Assert.assertEquals;

import com.lewis.timesheetservice.models.WorkingDay;
import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;
import java.util.GregorianCalendar;
import org.junit.Test;
import org.modelmapper.ModelMapper;

public class WorkingDayUT {

  private static final ModelMapper modelMapper = new ModelMapper();

  @Test
  public void checkWorkingDayMapping() {
    WorkingDayCreationDto creation = new WorkingDayCreationDto();
    creation.setDate(Date.valueOf("2021-08-21"));
    creation.setMorningStart(Time.valueOf("09:05:00"));
    creation.setMorningEnd(Time.valueOf("13:10:00"));
    creation.setAfternoonStart(Time.valueOf("14:00:00"));
    creation.setAfternoonEnd(Time.valueOf("17:00:00"));

    WorkingDay workingDay = modelMapper.map(creation, WorkingDay.class);

    assertEquals(creation.getMorningStart(), workingDay.getMorningStart());
    assertEquals(creation.getMorningEnd(), workingDay.getMorningEnd());
    assertEquals(creation.getAfternoonStart(), workingDay.getAfternoonStart());
    assertEquals(creation.getAfternoonEnd(), workingDay.getAfternoonEnd());

    WorkingDayUpdateDto update = new WorkingDayUpdateDto();
    update.setDate(Date.valueOf("2021-08-21"));
    update.setMorningStart(Time.valueOf("09:06:00"));
    update.setMorningEnd(Time.valueOf("13:00:00"));
    update.setAfternoonStart(Time.valueOf("14:05:00"));
    update.setAfternoonEnd(Time.valueOf("17:00:00"));

    modelMapper.map(update, workingDay);

    assertEquals(update.getMorningStart(), workingDay.getMorningStart());
    assertEquals(update.getMorningEnd(), workingDay.getMorningEnd());
    assertEquals(update.getAfternoonStart(), workingDay.getAfternoonStart());
    assertEquals(update.getAfternoonEnd(), workingDay.getAfternoonEnd());
  }
}
